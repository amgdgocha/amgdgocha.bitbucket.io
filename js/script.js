window.onload = function () {
  var hamburger = document.getElementsByClassName('hamburger')[0];
  var nav_mobile = document.getElementById('nav-mobile');

  hamburger.onclick = function () {

    var is_active = hamburger.classList.toggle('is-active');

    if (is_active === true) {
      nav_mobile.style.display = "block";
    }else {
      nav_mobile.style.display = "none";
    }
  };

  window.onresize = function () {
    if (window.innerWidth > 450) {
      nav_mobile.style.display = "none";
      hamburger.classList.remove('is-active');
    }
  }
};
